(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.betterTagsAutocompleteWidget = {
    attach: function (context, settings) {
      $(context).find('input[data-better-tags-parent^="better_tags--"]').once('betterTagsAutocompleteWidget').each(function() {
        var $this = $(this);
        var dataBT = $this.data('better-tags-parent');
        $('div[data-better-tags-container="' + dataBT + '"]')
          .append('JS is working')
          .prepend(settings.betterTags.Adam);
      })
    }
  }
}) (jQuery, Drupal, drupalSettings);