<?php

namespace Drupal\better_tags\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteTagsWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'better_tags_autocomplete' widget.
 *
 * @FieldWidget(
 *   id = "better_tags_autocomplete",
 *   label = @Translation("Better Tags (Autocomplete)"),
 *   description = @Translation("An autocomplete text field with tagging support and imporved UI."),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   multiple_values = TRUE
 * )
 */
class BetterTagsAutocompleteWidget extends EntityReferenceAutocompleteTagsWidget implements ContainerFactoryPluginInterface  {

  protected $id;
  protected $db;
  protected $entityManager;
  protected $entityInfo;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityManagerInterface $entityManager, Connection $database, EntityTypeBundleInfoInterface $entityInfo) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->db = $database;
    $this->entityInfo = $entityInfo;
    $this->entityManager = $entityManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity.manager'),
      $container->get('database'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $this->id = 'better_tags--' . $this->fieldDefinition->getName();
    $element['target_id']['#attributes']['data-better-tags-parent'] = $this->id;
    $element['beter_tags_target_id'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'data-better-tags-input' => $this->id,
      ],
    ];

    $container = $this->buildTagsContainer($items);

    $top_tags_container = $this->buildTopTagsContainer();

    if ($this->getSetting('better_tags_placement') == 'above' || $this->getSetting('better_tags_top_tags_placement') == 'above') {
      $label = $element['target_id']['#title'];
      unset($element['target_id']['#title']);
      $element['better_tags_container_label'] = [
        '#type' => 'label',
        '#weight' => -2,
        '#title' => $label,
      ];
    }
    if ($this->getSetting('better_tags_placement') == 'above') {

      $element['better_tags_container'] = $container;
      $element['better_tags_container']['#weight'] = 0;
      $element['target_id']['#weight'] = 1;

    }
    else {
      $element['better_tags_container'] = $container;
    }

    if ($this->getSetting('better_tags_top_tags_placement') == 'above') {

      $element['better_tags_top_tags_container'] = $top_tags_container;
      $element['better_tags_top_tags_container']['#weight'] = 1;
      $element['target_id']['#weight'] = 2;

    }
    else {
      $element['better_tags_top_tags_container'] = $top_tags_container;
    }



    $element['#attached']['library'][] = 'better_tags/better_tags';
    $element['#attached']['drupalSettings']['betterTags'] = [
      'Adam' => 'Blah Blah',
      'orientation' => $this->getSetting('better_tags_orientation'),
    ];

    return $element;
  }

  /**
   * Build the html container for our Better Tags.
   */
  private function buildTagsContainer($items) {
    foreach ($items->referencedEntities() as $item) {
      $tags[] = [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => $item->label(),
        '#attributes' => [
          'data-better-tags-parent' => $this->id,
          'data-better-tags-item-id' => $item->id(),
          'class' => [
            'better-tags-tag',
          ]
        ],
      ];
    }
    if ($this->getSetting('better_tags_orientation') == 'horizontal') {
      $tags_html = render($tags);
      $container = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => $tags_html,
      '#attributes' => [
        'data-better-tags-container' => $this->id,
        'class' => [
          'better-tags-horizontal-tags',
        ]
      ],
    ];
    }
    else {
      $container = [
        '#theme' => 'item_list',
        '#items' => $tags,
        '#attributes' => [
          'data-better-tags-container' => $this->id,
          'class' => [
            'better-tags-vertical-tags',
            'links',
          ]
        ],
      ];
    }
    return $container;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'better_tags_placement' => 'below',
      'better_tags_orientation' => 'horizontal',
      'better_tags_top_tags' => FALSE,
      'better_tags_top_tags_count' => 5,
      'better_tags_top_tags_placement' => 'below',
      'better_tags_top_tags_orientation' => 'vertical',
     ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $label = $this->getTargetLabel();

    $element['better_tags_placement'] = [
      '#title' => $this->t('Placement of @tags', array('@tags' => $label)),
      '#type' => 'select',
      '#options' => [
        'above' => $this->t('Above'),
        'below' => $this->t('Below'),
      ],
      '#default_value' => $this->getSetting('better_tags_placement'),
    ];
    $element['better_tags_orientation'] = [
      '#title' => $this->t('Orientation of @tags', array('@tags' => $label)),
      '#type' => 'select',
      '#options' => [
        'horizontal' => $this->t('Horizontal (Tags)'),
        'vertical' => $this->t('Vertical (List)'),
      ],
      '#default_value' => $this->getSetting('better_tags_orientation'),
    ];
    $id = 'better_tags_top_tags--' . $this->fieldDefinition->getName();
    $element['better_tags_top_tags'] = [
      '#title' => $this->t('Show Top @tags', array('@tags' => $label)),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('better_tags_top_tags'),
      '#attributes' => [
        'data-better_tags_top_tags' => [$id],
      ],
    ];
    $states = [
      '#states' => [
        // Only show fields if the 'better_tags_top_tags' checkbox is enabled.
        'visible' => [
          ':input[data-better_tags_top_tags="' . $id . '"]' => ['checked' => TRUE],
        ],
      ],
    ];
    // Set up out number options array.
    for ($i = 10; $i > 0; $i--) {
      $count[$i] = $i;
    }
    $element['better_tags_top_tags_count'] = [
      '#title' => $this->t('Number of top @tags to show', array('@tags' => $label)),
      '#type' => 'select',
      '#options' => $count,
      '#default_value' => $this->getSetting('better_tags_top_tags_count'),
    ] + $states;
    $element['better_tags_top_tags_placement'] = [
      '#title' => $this->t('Placement of @tags', array('@tags' => $label)),
      '#type' => 'select',
      '#options' => [
        'above' => $this->t('Above'),
        'below' => $this->t('Below'),
      ],
      '#default_value' => $this->getSetting('better_tags_top_tags_placement'),
    ] + $states;
    $element['better_tags_top_tags_orientation'] = [
      '#title' => $this->t('Orientation of @tags', array('@tags' => $label)),
      '#type' => 'select',
      '#options' => [
        'horizontal' => t('Horizontal (Comma Seperated)'),
        'vertical' => t('Vertical (List)'),
      ],
      '#default_value' => $this->getSetting('better_tags_top_tags_orientation'),
    ] + $states;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $placement = $this->getPlacements();
    $label = $this->getTargetLabel();
    $summary[] = $this->t('Selected @tags will be shown @orientation, @placement the field.', [
        '@tags' => $label,
        '@orientation' => $placement[$this->getSetting('better_tags_orientation')],
        '@placement' => $placement[$this->getSetting('better_tags_placement')],
      ]);
    if ($this->getSetting('better_tags_top_tags')) {
      $summary[] = $this->t('Top @tags will show @num @tags @orientation, @placement the field.', [
        '@tags' => $label,
        '@num' => $this->getSetting('better_tags_top_tags_count'),
        '@orientation' => $placement[$this->getSetting('better_tags_top_tags_orientation')],
        '@placement' => $placement[$this->getSetting('better_tags_top_tags_placement')],
      ]);
    }

    return $summary;
  }

  /**
   * Return a string of target labels for display.
   */
  private function getTargetLabel($lower = NULL) {
    $bundle_info = $this->entityInfo->getBundleInfo($this->fieldDefinition->getSetting('target_type'));
    $bundles = $this->fieldDefinition->getSetting('handler_settings')['target_bundles'];
    foreach ($bundles as $bundle) {
      $labels[] = $bundle_info[$bundle]['label'];
    }
    $labels = implode(',', $labels);
    return ($lower) ? strtotlower($labels) : $labels;
  }

  /**
   * Return an array of placments.
   */
  private function getPlacements() {
    return [
      'horizontal' => $this->t('horizontally'),
      'vertical' => $this->t('vertically'),
      'above' => $this->t('above'),
      'below' => $this->t('below'),
    ];
  }

  /**
   * Gets the table name for the field to pull top tags.
   */
  private function getFieldTable() {
    $type = $this->fieldDefinition->getTargetEntityTypeId();
    $field_name = $this->fieldDefinition->getName();
    $table_name = "{$type}___{$field_name}";
    //return $table_name;
    if (!$this->db->schema()->tableExists($table_name)) {
      $name = $type . '.field_schema_data.' . $field_name;
      $query = $this->db
        ->select('key_value', 'kv')
        ->fields('kv');
      $query
        ->condition('kv.collection', 'entity.storage_schema.sql')
        ->condition('kv.name', $name);
      $items = $query
        ->execute()
        ->fetchAll();
      $data = array_shift($items);
      if (!empty($data->value)) {
        $value = unserialize(($data->value));
        $table_name = array_shift(array_keys($value));
      }
    }

    return $table_name;
  }

  /**
   * Builds out the list of top tags and then formats them for display.
   */
  private function buildTopTagsContainer() {
    $target_id = $this->fieldDefinition->getName() . '_target_id';
    $table = $this->getFieldTable();
    $query = $this->db->select($table, 'bt');
    $query->addField('bt', $target_id, 'id');
    $query->addExpression('COUNT(:id)', 'total', [':id' => $target_id]);
    $query->addExpression('SUM(delta)','score');
    $query->groupBy('id');

    $results = $query->orderBy('total', 'DESC')
      ->orderBy('score', 'ASC')
      ->range(0, $this->getSetting('better_tags_top_tags_count'))
      ->execute()
      ->fetchAllAssoc('id');

    if ($results) {
      $tags_storage = $this->entityManager->getStorage($this->fieldDefinition->getSetting('target_type'));

      $items = $tags_storage->loadMultiple(array_keys($results));
      $label = $this->getTargetLabel();
      $element = [
       '#type' => 'details',
       '#open' => FALSE,
       '#attributes' => [
          'data-better-tags-top-tags' => $this->id,
        ],
       '#title' => $this->t('Show Top Used @tags', ['@tags' => $label]),
      ];

      foreach ($items as $item) {
        $tags[] = [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => $item->label(),
          '#attributes' => [
            'data-better-top-tags-parent' => $this->id,
            'data-better-top-tags-item-id' => $item->id(),
            'class' => [
              'better-tags-tag',
            ]
          ],
        ];
      }

      if ($this->getSetting('better_tags_orientation') == 'horizontal') {
        $tags_html = render($tags);
        $container = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $tags_html,
        '#attributes' => [
          'data-better-top-tags-container' => $this->id,
          'class' => [
            'better-tags-horizontal-tags',
          ]
        ],
      ];
      }
      else {
        $container = [
          '#theme' => 'item_list',
          '#items' => $tags,
          '#list_type' => 'ol',
          '#attributes' => [
            'data-better-top-tags-container' => $this->id,
            'class' => [
              'better-tags-vertical-tags',
              'links',
            ]
          ],
        ];
      }
      if (!empty($container)) {
        $element['better_tags_top_tags'] = $container;
        return $element;
      }
    }
  }
}
